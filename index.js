 var web = require('./websites.json');
  var os = require('os');
 var fs = require('fs');
 var request = require('request');
 var rp = require('request-promise');
 var argv = require('minimist')(process.argv.slice(2));
 console.dir(argv);
 var google;
 var to = 60000;

 var dnsleaktesturl = "http://45.33.96.102:8080/"




 
 if (argv.hasOwnProperty('w')) {
     // console.dir("Hitting all website of  ", argv.w);
     web = require('./' + argv.w);
 }
 if (argv.hasOwnProperty('t')) {
     //console.dir("Timeout taken in ms  ", argv.t);
     to = argv.t;
 }
 if (argv.hasOwnProperty('n')) {
     //console.dir("Timeout taken in ms  ", argv.t);
     serverIp = argv.n;
 }

 var url = 'http://www.google.com';
 request({ url: url, followRedirect: false }, function (err, res, body) {
   console.log(res.headers.location);
   google = res.headers.location;
 });
 

 var totalWebsites = 0;
 var fail = 0;
 var pass = 0;
 var finalFailReport = [];
 var finalPassReport = [];
 web.websites.forEach(function (element) {
     //getWebsiteDetails(element.domain);
     var opts = {
         uri: addhttp(element.domain),
         followAllRedirects: true,
         agentOptions: {
             keepAlive: true,
             keepAliveMsecs: to
         },
         timeout: to,
         headers: {
             'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
         }
     };
     rp(opts)
         .then(function (body) {
             pass++;
             totalWebsites++;
             isDone();
             var re = new RegExp("<title>(.*?)</title>", "i");
             var match = body.match(re);
             if (match && match[1]) {
                 console.log("------------------------title", match[1]);
                 finalPassReport.push({
                     title: match[1],
                     url: opts.uri
                 })
             }

         })
         .catch(function (err) {
             fail++;
             totalWebsites++;
             isDone();
             finalFailReport.push({
                 errorName: err.name,
                 errorCode: err.statusCode,
                 errorCause: err.cause,
                 url: opts.uri
             });
         });
 }, this);

 function isDone() {
     console.log(totalWebsites);
     if (web.websites.length == totalWebsites) {
         done();
     }
 }

 function done() {
     console.log("Result for server ip:", serverIp)
     console.log("Total fail:", fail)
     console.log("Total Pass:", pass)
     console.log("Total :", totalWebsites)
     console.log("Timeout :", to)
     console.log(finalFailReport)
     var report = {};
     report.name = "Report for " + serverIp;
     report.finalFailReport = finalFailReport;
     report.fail = fail;
     report.pass = pass;
     report.totalWebsites = totalWebsites;
     report.timeout = to;
     report.google = google;
     fs.writeFile(process.cwd() + "/results/result_" + serverIp + ".json", JSON.stringify(report), function (err) {
         if (err) {
             return console.log(err);
         }
     });
 }


 function addhttp(url) {
     url = url.toLowerCase();
     if (!/^(?:f|ht)tps?\:\/\//.test(url)) {
         url = "http://" + url;
     }
     return url;
 }





 