# (is-reachable) 

Steps to use

 * install [Termux](https://play.google.com/store/apps/details?id=com.termux) in android
 * Install node.js on android run these command on termux
 ```
 apt update && apt upgrade
 ```
 ```
 apt install coreutils
 ```
 ```
 apt install nodejs
 ```
  ```
 apt install git
 ```
 * Clone nodedroid repo and run 
 ```
 git clone https://bitbucket.org/nitingurbani/nodedroid/
 ```
 ```
 cd nodedroid
 ```
 ```
 npm install
 ```
 ```
 node index.js
 ```
 * to add more website add it in website.json
 * Termux tips (Ctrl+C = volume down + c and TAB = volume up + T)

### update
 ``` 
 git pull
 ```
 ```
 npm update
 ```

 
### To run URL test:
Eg. to run all websites in korea.json :
 ```
 node index.js -w=korea.json
 ```

 * To change timeout, use -t and give timeoput in ms
 ```
 node index.js -t=9000
 ```
 * 
 ```
  node index.js -w=korea.json -t=9000
 ```

* Name your test results with -n
 ```
 node index.js -w=korea.json -n=9.9.9.9
 ```

### To run DnsLeak test:

node dnsleak.js -n=89.89.89.89


### To run Ad Blocker test:

 node index.js -w=adsites.json -n=VPN_name




 
 


### Pending stuff:

 * send report to slack or email or create json file
 

